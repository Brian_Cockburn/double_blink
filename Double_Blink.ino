/*
  Double Blink
  ============

  Turns an LED on for one second, then off for one second, repeatedly.

  Most Arduinos have an on-board LED you can control. On the UNO, MEGA and ZERO
  it is attached to digital pin 13, on MKR1000 on pin 6. LED_BUILTIN is set to
  the correct LED pin independent of which board is used.
  If you want to know what pin the on-board LED is connected to on your Arduino
  model, check the Technical Specs of your board at:
  https://www.arduino.cc/en/Main/Products

  modified 8 May 2014 - by Scott Fitzgerald
  modified 2 Sep 2016 - by Arturo Guadalupi
  modified 8 Sep 2016 - by Colby Newman

  This example code is in the public domain.

  http://www.arduino.cc/en/Tutorial/Blink
*/

// Modified by Brian Cockburn to give a distinctive double blink 
// pattern.  In the case of the Pro Micro it blinks the two LEDs 
// on the board - Tx and Rx.

#if defined(ARDUINO_AVR_NANO) || defined(ARDUINO_SEEED_XIAO_M0) 
  // Nothing needed for a simple case
#elif defined(ARDUINO_AVR_PROMICRO)
  // This is from Sparkfun doco
  // const int RXLED = 17;
  // Note that on MY Pro Micro boards from China
  // the TX and RX LEDs are BOTH RED
  // Define RX LED 'functions' to match the TX LED ones
  // #define RXLED1 /* ON  */ do{digitalWrite(RXLED, LOW);}while(0)
  // #define RXLED0 /* OFF */ do{digitalWrite(RXLED, HIGH);}while(0)
  // It would seem that there already exist macros for this that aren't 
  // immediately obvious from the SparkFun documents
#else
  #error "Unknown Platform"
#endif

// the setup function runs once when you press reset or power the board
void setup() {
#if defined(ARDUINO_AVR_NANO) || defined(ARDUINO_SEEED_XIAO_M0) 
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(LED_BUILTIN, OUTPUT);
#elif defined(ARDUINO_AVR_PROMICRO)
  // Note that this LED is inverted compared to Uno
  TX_RX_LED_INIT;
#endif
}

// the loop function runs over and over again forever
void loop() {
#if defined(ARDUINO_AVR_NANO) || defined(ARDUINO_SEEED_XIAO_M0) 
  digitalWrite(LED_BUILTIN, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(300);                        // wait for a bit
  digitalWrite(LED_BUILTIN, LOW);    // turn the LED off by making the voltage LOW
  delay(300);                        // wait for a bit
  digitalWrite(LED_BUILTIN, HIGH);   // turn the LED on by making the voltage HIGH
  delay(300);                        // wait for a bit
  digitalWrite(LED_BUILTIN, LOW);    // turn the LED off by making the voltage LOW
  delay(1000);                       // wait for a second
#elif defined(ARDUINO_AVR_PROMICRO)
  // Double blink RX LED
  RXLED1;  delay(200);
  RXLED0;  delay(100);
  RXLED1;  delay(200);
  RXLED0;  delay(300);
  // Triple blink TX LED, T for TX and T for triple
  TXLED1;  delay(150);
  TXLED0;  delay(75);
  TXLED1;  delay(150);
  TXLED0;  delay(75);
  TXLED1;  delay(150);
  TXLED0;  delay(300);
#endif
}
